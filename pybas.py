#!/usr/bin/env python

from pyparsing import *

# GLOBAL VARIABLES FOR INDENT LEVEL
indent = ''
indent_size = 2

# IN FUNCTION?
inFunc = False
funcName = ''

# DICT HOLDING TYPE DECORATORS
rdict = dict( zip( ('$','!','#'),('std::string','int','float') ) )

# KEYWORDS
DIM     = Keyword('DIM',    caseless=True)
LET     = Keyword('LET',    caseless=True)
PRINT   = Keyword('PRINT',  caseless=True)
FOR     = Keyword('FOR',    caseless=True)
TO      = Keyword('TO',     caseless=True)
STEP    = Keyword('STEP',   caseless=True)
NEXT    = Keyword('NEXT',   caseless=True)
IF      = Keyword('IF',     caseless=True)
THEN    = Keyword('THEN',   caseless=True)
ELSE    = Keyword('ELSE',   caseless=True)
ENDIF   = Keyword('END IF', caseless=True)
WHILE   = Keyword('WHILE',  caseless=True)
WEND    = Keyword('WEND',   caseless=True)
END     = Keyword('END',    caseless=True)
ASC     = Keyword('ASC',    caseless=True)
VAL     = Keyword('VAL',    caseless=True)
STR     = Keyword('STR',    caseless=True)
MID     = Keyword('MID',    caseless=True)
LEFT    = Keyword('LEFT',   caseless=True)
RIGHT   = Keyword('RIGHT',  caseless=True)
UCASE   = Keyword('UCASE',  caseless=True)
LCASE   = Keyword('LCASE',  caseless=True)
UBOUND  = Keyword('UBOUND', caseless=True)
LBOUND  = Keyword('LBOUND', caseless=True)
FUNCTION= Keyword('FUNCTION',caseless=True)
RETURN  = Keyword('RETURN', caseless=True)
ENDFUNC = Keyword('END FUNCTION', caseless=True)
SUB     = Keyword('SUB',    caseless=True)
ENDSUB  = Keyword('END SUB', caseless=True)
AS      = Keyword('AS',     caseless=True)
IMPORT  = Keyword('IMPORT', caseless=True)

# FILE I/O
EXISTS  = Keyword('EXISTS', caseless=True)
OPEN    = Keyword('OPEN',   caseless=True)
CLOSE   = Keyword('CLOSE',  caseless=True)
READFILE= Keyword('READFILE',caseless=True)
WRITEFILE=Keyword('WRITEFILE',caseless=True)




# VARIABLE TYPES

# CALLBACKS
def do_DEDENT(s,l,t):
    global indent
    indent = indent[:-indent_size]
    return indent + '}'

def do_IMPORT(s,l,t):
    Imports = t[0]+ ' '
    global indent, rdict
    LF = ''
    if len(t[1]) > 1: LF = '\n'
    for Import in t[1]:
        Imports += Import +','
    return Imports[:-1]    
    

    
def do_ASSIGN(s,l,t):
    global indent, inFunc, funcName
    
    if inFunc == True and funcName == t[0]:
        inFunc = False
        funcName = ''
        return indent + ' '.join( ('return', t[2]) )
        
    if len(t[2])>1: t[2] = ''.join(t[2])
    if t[1] == '&=': t[1] = '+='
    return indent + ' '.join( (t[0],t[1],t[2]) )

def do_FCALL(s,l,t):
    '''
    Re-Writes specific Functions Calls,
    With Generic Function Call Handler at End
    '''
    global indent, rdict
    keys = tuple(rdict.keys())
    if t[0].endswith(keys): t[0] = t[0][:-1]
    if t[0] == ASC:
		return t[1][0].replace('"',"'"),";"
    elif t[0] == UCASE:
		return ''.join( (t[0],'(',t[1][0],');') )
    elif t[0] == LCASE:
		return ''.join( (t[0],'(',t[1][0],');') )
    elif t[0] == LEFT:
		endp = str( int(t[1][1])-1 )
		return ''.join( (t[0],'(',t[1][0],',',endp,');') )
    elif t[0] == MID:
        string = t[1][0]
        startp = t[1][1]
        endp = str( int(startp)+1 + int(t[1][2]))
        return ''.join( (t[0],"(",string,',',startp,',',endp,')') )

    elif t[0] == RIGHT:
        string = t[1][0]
        startp = t[1][1]
        endp = str( len(string) - int(t[1][1]))
        return ''.join( (t[0],"(",string,',',endp,')') )
        
    elif t[0] == EXISTS:
        return ''.join( ( 'Exists', '(', t[1][0], ')') ) 
        return ''

    elif t[0] == READFILE:
        return ''.join( ( 'LoadFile', '(', t[1][0], ')') )
        
    elif t[0] == WRITEFILE:
        return ''.join( ( 'SaveFile', '(', t[1][0], ',', t[1][1], ')') )        
        
    else: # GENERIC FUNCTION CALL HANDLER
        global indent
        parameters = ''
        for param in t[1]:
            parameters += param+", " 
        return indent + ' '.join( (t[0], '(', parameters[:-2], ')') )
        
def do_LET(s,l,t):
    '''
        Converts LET to VAR
    '''
    if len(t[3]) > 1: t[3] = ''.join(t[3])
    return indent + ' '.join( (rdict[t[1][-1]],t[1][:-1],t[2],t[3]) )+';'

def do_DIM(s,l,t):
    '''
        Converts DIM$/!/# to appropriate declaration
    '''
    varNames = ''
    global indent, rdict
    LF = ''
    if len(t[1]) > 1: LF = '\n'
    for varName in t[1]:
		varNames += indent + ''.join( (rdict[varName[-1]], ' ', varName[:-1],';', LF) )
    return varNames
    
def do_PRINT(s,l,t):
    '''
        Converts PRINT to STD::COUT
    '''
    global indent
    return  indent + ''.join( ('std::cout << ', t[1],';') )

def do_FOR(s,l,t):
    '''
        Converts Standard FOR/STEP Loop
        for x = 10 to 0 step -2
        for x = 0 to 10 step 1
    '''
    global indent
    line_end ='):'
    op = '+='
    if t[6]:
        if '-' in t[7]:
            op = '-='

    tmp = indent + ''.join( (t[0].lower(), ' ( ', t[1], '=', t[3], '; ', t[1], '<', t[5],'; ', t[1], op, t[7], ' ) {') )
    indent += ' '*indent_size
    return tmp

def do_IF(s,l,t):
    '''
        Converts IF/THEN
        Changes "=' to "=="
        Changes "<> to "!="
    '''
    global indent
    if len(t) > 2:
        if t[2] == '=': t[2] = '=='
        if t[2] == '<>': t[2] = '!='
        tmp = indent + ' '.join( ( t[0].lower(),'( ',t[1],t[2], t[3],' ) {') )
    else:
        tmp = indent + ' '.join( (t[0].lower(), '(', t[1]) )+' ) {'
    indent += ' '*indent_size
    return tmp

def do_ELSE(s,l,t):
    '''
        Handles ELSE Statement
    '''
    global indent
    indent = indent[:-indent_size]
    tmp = indent + "} \n" + indent + t[0].lower() +" {"
    indent += ' '*indent_size
    return tmp

def do_WHILE(s,l,t):
    '''
        Handles WHILE/WEND
    '''
    global indent
    tmp = indent + ' '.join( (t[0].lower(),'(',t[1],t[2], t[3],') {') )
    indent += ' '*indent_size
    return tmp

def do_FUNCSUB(s,l,t):
    '''
        Converts USER FUNCTION/SUB Creation.
        Does not handle forward declarations
    '''
    global indent, inFunc, funcName,rdict
    
    retType = "Int"
    
    indent += ' '*indent_size
    
    parameters = ''
    
    for param in t[2]:
        if len(t[2][0]) > 1:
            parameters += param[0]+": "+param[2]+", " 
        else:
            parameters += param[0]+", "
    x = t[2][0]

    if t[0] == SUB:        
        return ' '.join ( ('PROC', t[1], '(', parameters[:-2], '): string {.discardable.} =') )
    elif t[0] == FUNCTION:
        inFunc = True
        funcName = t[1]
#         print "FuncName", funcName, funcName.endswith(keys)
        if funcName.endswith(tuple(rdict.keys())):
            return ' '.join( ('PROC',funcName[:-1], '(', parameters[:-2], '): ', rdict[funcName[-1]], '=') )

        else:
            return ' '.join ( ('PROC', t[1], '(', parameters[:-2], '): ', t[4], '=') )
    return ''

def do_RETURN(s,l,t):
    '''
        Handles Function RETURN statement
    '''
    global indent
    return indent + ' '.join( (t[0],t[1]) )


# *** GRAMMER BEGINS HERE ***

expr = Forward() # This makes the Grammer RECURSIVE.

# SUPPRESS INCLUSION OF () IN STEAM
LPAR,RPAR = map(Suppress, "()")

# BASE IDENTIFIER
IDENT = Word(alphas+"_",alphanums+"_")

# SBSTRIDENT = Word(alphas+"_",alphanums+"$_")
# SBINTIDENT = Word(alphas+"_",alphanums+"!_")
# SBFLOIDENT = Word(alphas+"_",alphanums+"#_")
SBIDENT = (Word(alphas+"_",alphanums+"$!#@_"))

# USER CALLED FUNCTION SIGNATURE
FCALL = (SBIDENT | IDENT) + LPAR + Group(Optional(delimitedList(expr))) + RPAR

# NUMERIC SIGNATURES
INTEGER = Regex(r"-?\d+")
real = Regex(r"-?\d+\.\d*")

# STRING SIGNATURE
string = dblQuotedString

# COMPARISON SIGNATURES
EQU = Literal('=')
LT = Literal( '<' )
GT = Literal( '>' )
NEQ = Literal( '<>' )
LTE = Combine(LT, EQU)
GTE = Combine(GT, EQU)
PLEQU = Literal('+=')
TIMEQU = Literal('*=')

# ASSIGNMENT OPERATORS, "COMBINED" TOGETHER
ASSIGNOP = (NEQ | LTE | GTE | PLEQU | TIMEQU | EQU | LT | GT )

# IDENTIFIER FOR RVALUES OF "FOR" STATEMENT
FORIDENT = ( IDENT | INTEGER )

# FOR COMMAND SIGNATURE
# "STEP" IS OPTIONAL
FORCMD = FOR + IDENT + EQU + FORIDENT + TO + FORIDENT + Optional(STEP + FORIDENT)

# LET COMMAND SIGNATURE
LETCMD  = LET + (SBIDENT ^ IDENT) + EQU + (expr ^ restOfLine)

# DIM COMMAND SIGNATURE
DIMCMD = DIM + Group(SBIDENT + ZeroOrMore(Suppress(',') + SBIDENT))

# IF COMMAND SIGNATURE
# "THEN" IS NOT MATCHED,
# BECAUSE IT'S NOT NEEDED IN
# THIS IMPLEMENTATION
IFCMD = IF + (FCALL ^ IDENT) + Optional(ASSIGNOP + expr)

# ELSE STATEMENT SIGNATURE
# "ELSE" SHOULD BE ON IT'S OWN LINE
ELSECMD = ELSE + Empty()

# PRINT COMMAND SIGNATURE
PRINTCMD = PRINT + expr

# WHILE COMMAND SIGNATURE
WHILECMD = WHILE + IDENT + ASSIGNOP + expr

# FILE I/O COMMANDS
EXISTSCMD   = EXISTS + expr
OPENCMD     = OPEN + expr
CLOSECMD    = EXISTS + expr
READFILECMD = EXISTS + expr
WRITEFILECMD= EXISTS + expr +expr
FILEIOCMD = (EXISTSCMD ^ OPENCMD ^ CLOSECMD ^ READFILECMD ^ WRITEFILECMD)


# SUB AND FUNCTION COMMANDS
# PARAMETERS ARE OPTIONAL
# IN THIS SIGNATURE
SUBCMD = SUB + (SBIDENT | IDENT) + LPAR + Group(Optional(delimitedList(expr))) + RPAR
FUNCCMD = FUNCTION + (IDENT) + LPAR + Group(Optional(delimitedList(expr))) + RPAR + AS + IDENT
SBFUNCCMD = FUNCTION + (SBIDENT |IDENT) + LPAR + Group(Optional(delimitedList(expr))) + RPAR
FUNCSUBCMD = (SBFUNCCMD ^ FUNCCMD ^ SUBCMD)
IMPORTCMD = IMPORT + Group(IDENT + ZeroOrMore(Suppress(',') + IDENT))

# PARAMETERS FOR FUNCTION/SUB STATEMENTS
# GROUPED FOR EASIER PARSING.
# INSTEAD OF a, b, c WE GET
# [a,b,c] GROUPED IN IT'S OWN LIST

PARAMTYPE =  Group(IDENT + AS + IDENT)

# RETURN COMMAND SIGNATURE
# VALUE TO RETURN IS OPTIONAL
RETURNCMD =  RETURN + Optional(expr)

# COMMAND REQUIRING REMOVAL OF ONE LEVEL OF INDENTATION
DEDENT = ENDIF | WEND | NEXT | ENDFUNC | ENDSUB

# VARIABLE DECLARATION SIGNATURE
# HANDLES REGULAR ASSIGNMENT AS
# WELL AS "variable[1]" SYNTAX
vardecl = LET + (SBIDENT ^ IDENT) + ZeroOrMore("[" + expr + "]")

# VARIABLE REFERENCE SIGNATURE
# SAME AS ABOVE, BUT WHEN *USING*
# THE VARIABLE
varref = (SBIDENT |IDENT) + ZeroOrMore("[" + expr + "]")

# LEFT SIDE OF ASSIGNMENT
lhs = vardecl | varref # Either a Variable Declaration, or a reference to a Variable.

# RIGHT SIDE OF ASSIGNMENT
rhs = expr

# VARIABLE ASSIGNMENT SIGNATURE
ASSIGNMENT = lhs + ASSIGNOP + rhs

# THESE ARE ALL THE GRAMMER RULES THAT WILL BE MATCHED
# *************************
# **** ORDER MATTERS!! ****
# *************************
operand = (  FUNCSUBCMD | PARAMTYPE | FCALL | FILEIOCMD| IFCMD | ELSECMD | PRINTCMD | WHILECMD | LETCMD | DIMCMD | IMPORTCMD | DEDENT | FORCMD | RETURNCMD | ASSIGNMENT | IDENT | real | INTEGER | string | ASSIGNOP )

# EXPRESSION: WHAT CONSTITUTES A MATCH IN CODE
# INCLUDES SIMPLE OPERATOR PRECEDENCE
expr <<= operatorPrecedence(operand,
    [
    (oneOf("* / %"), 2, opAssoc.LEFT),
    (oneOf("+ -"), 2, opAssoc.LEFT),
    ])

# JUST FOR CLARITY
Program = expr

# SET UP CALLBACKS FOR SPECIFIC RULES WHERE RE-WRITING OUTPUT IS REQUIRED
LETCMD .setParseAction(do_LET)
DIMCMD.setParseAction(do_DIM)
IMPORTCMD.setParseAction(do_IMPORT)
PRINTCMD.setParseAction(do_PRINT)
FORCMD.setParseAction(do_FOR)
IFCMD.setParseAction(do_IF)
ELSECMD.setParseAction(do_ELSE)
WHILECMD.setParseAction(do_WHILE)
DEDENT.setParseAction(do_DEDENT)
ASSIGNMENT.setParseAction(do_ASSIGN)
FCALL.setParseAction(do_FCALL)
FUNCSUBCMD.setParseAction(do_FUNCSUB)
RETURNCMD.setParseAction(do_RETURN)

# TEST CODE SNIPPET
# IF IT TRANSLATES, COMPILES, AND RUNS WITH NIMROD
# THEN WE'RE GOOD
test1 = """\
    LET z = "Goodbye."
    print "Hello!"
    
    for x = 0 to 10
        IF x <> 0 THEN
            print x
        else
            print "Done."
        END if
    next
    
    let x = 0
    while x < 10
        print x
        x = x + 1
    wend
    
    let b = asc("a")
    let c = UCASE("uppercase")
    let d = LCASE("LOWERCASE")
    
    print b
    print c
    print d
    print z
    print LEFT("Hello",4)
    print MID("Hello, World",7,5)
    print RIGHT$("Hello, World",7)
    
    Sub easy(a,b,c,d)
        print "Hello from a SUB"
    end sub
    
    Function float_test!(e)
        let b = 2
        let x = e + 5.5 
        float_test! = x
    End Function
    
    easy("foo",1,"bar",True)
    print float_test(1.1)
    dim me$
    dim f!
    f = 10
    Let q = "Quit"
    Print q
"""

test2 = '''\

    dim a!
    dim b#
    dim c$
    
    a = 123
    b  = 1.23
    c = "A dynamic string"
    
    print a
    print b
    print c

    dim g$, h!,j#
    
    if Exists("pybas2nim.py") then
        g = ReadFile("pybas2nim.py")
        print g
    end if

    WriteFile("source.txt",g)
    
    Let text$ = "Hello"
    lEt x! = 1
    lET y# = 2.200

'''

test3 = '''\
    for x = 0 to 10 step 1
        IF x <> 0 THEN
            print x
        else
            print "Done."
        END if
    next
'''
# START FEEDING THE PARSER
# THIS COULD BE DONE FROM A FILE
# BUT IT'S SIMPLER TO TEST WITH
# CODE SNIPPET ABOVE

# print "IMPORT StrUtils"
for t in test2.splitlines():
    if len(t.strip()):
        print ''.join(Program.searchString(t)[0])

